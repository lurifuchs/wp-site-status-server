<div class="card">
    <div class="card-header site-header d-flex justify-content-between">
        Your site
        <div>
            <a><i class="ml-3 fa fa-cog"></i></a>
            <a title="Visit site"><i class="ml-1 fas fa-external-link-alt"></i></a>
        </div>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">PHP</span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">PHP</span>
            <span class="badge badge-success" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - PHP</small>">7.3.6</span>
        </div>

        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">WordPress</span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">WordPress</span>
            <span class="badge badge-success" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - WordPress</small>">5.2.1</span>
        </div>

        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">Yoast SEO </span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">Yoast SEO</span>
            <span class="plugin-status-badge badge badge-danger" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - Yoast SEO</small>">
                11.4
                <i class="fa fa-arrow-right"></i> 12.3
            </span>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">Disable Gutenberg </span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">Disable Gutenberg</span>
            <span class="plugin-status-badge badge badge-warning" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - Disable Gutenberg</small>">
                1.8.1
                <i class="fa fa-arrow-right"></i> 1.9
            </span>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">Contact Form 7 </span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">Contact Form 7</span>
            <span class="plugin-status-badge badge badge-warning" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - Contact Form 7</small>">
                5.1.3
                <i class="fa fa-arrow-right"></i> 5.1.4
            </span>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">Advanced Custom Fields </span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">Advanced Custom Fields</span>
            <span class="plugin-status-badge badge badge-warning" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - Advanced Custom Fields</small>">
                5.8.1
                <i class="fa fa-arrow-right"></i> 5.8.5
            </span>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-2">
            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">WP Site Status Client </span>
            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">WP Site Status Client</span>
            <span class="plugin-status-badge badge badge-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<small>Your site - WP Site Status Client</small>">
                1.1.0
            </span>
        </div>
    </div>
</div>