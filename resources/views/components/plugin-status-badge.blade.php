<span class="plugin-status-badge badge badge-{{ $plugin->getStatusAsString($site->id) }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name }} - {{ $plugin->name }}</small>">
    @if ($plugin->isInstalledOnNetwork($site->id))
        <span class="fa fa-globe" aria-hidden="true" title="Network activated"></span>
    @endif

    {{ $plugin->getVersion($site->id) }}

    @if ($plugin->latest_version && $plugin->latest_version != $plugin->getVersion($site->id))
        <i class="fa fa-arrow-right"></i> {{ $plugin->latest_version }}
    @endif
</span>
