<div class="d-none d-lg-flex flex-grow-1 align-items-center w-100">
    <ul class="navbar-nav mr-auto ml-5">
        <li class="nav-item">
            <a class="nav-link" href="/features">Features</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/documentation">Documentation</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/download-plugin">Download plugin</a>
        </li>
    </ul>

    <ul class="navbar-nav">
        @if (Auth::check())
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user-circle"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/dashboard">Dashboard</a>
                    <a class="dropdown-item" href="/account">Account</a>

                    @if (isset($user) && $user && (bool) $user->is_admin)
                        <a class="dropdown-item" href="/admin">Admin</a>
                    @endif

                    <a class="dropdown-item" href="/logout">Logout</a>
                </div>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link" href="/login"><i class="fa fa-sign-in-alt"></i> Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/register"><i class="fa fa-user"></i> Register</a>
            </li>
        @endif
    </ul>
</div>