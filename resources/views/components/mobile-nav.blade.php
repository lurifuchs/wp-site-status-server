<div class="collapse navbar-collapse" id="navbar">
    <div class="container d-lg-none">
        <div class="row">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/features">Features</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/documentation">Documentation</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/download-plugin">Download plugin</a>
                </li>

                @if (isset($user) && $user && (bool) $user->is_admin)
                    <li class="nav-item mt-5 mb-2"><b>Admin</b></li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin"><i class="fa fa-user"></i> Admin</a>
                    </li>
                @endif

                @if (Auth::check())
                    <li class="nav-item mt-5 mb-2"><b>Account</b></li>
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/account"><i class="fa fa-sign-in-alt"></i> Account</a>
                    </li>
                    <li class="nav-item border-bottom">
                        <a class="nav-link" href="/logout"><i class="fa fa-user"></i> Logout</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="/login"><i class="fa fa-sign-in-alt"></i> Login</a>
                    </li>
                    <li class="nav-item border-bottom">
                        <a class="nav-link" href="/register"><i class="fa fa-user"></i> Register</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>