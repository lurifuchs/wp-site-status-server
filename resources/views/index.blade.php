@extends('layout')

@section('title', 'A WordPress Management Dashboard.')

@section('container-fluid')
    <section class="hero colorscheme--yellow">
        <div class="container text-center text-lg-left">
            <h1>WordPress Management Dashboard</h1>
            <div class="row d-flex align-items-center">
                <div class="col-12 col-lg-6 order-2 order-lg-1">
                    <p>WP Site Status notifies you when updates are available and backup your database. It helps you keep your site up to date and secure.</p>
                    <p><a href="/register" class="mt-3 link-button">Create your free account</a></p>
                </div>
                <div class="col-12 col-lg-6 order-1 order-lg-2 legible">
                    <img class="rounded-circle" src="/images/screenshot-square.png" alt="Screenshot of dashboard">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row legible px-5">
                <div class="col-12 col-lg-6 order-2 order-lg-1">
                    @include('components.example-card')
                </div>
                <div class="col-12 col-lg-6 order-1 order-lg-2 legible justify-content-center">
                    <div class="mb-5">
                        <h2>Clear overview</h2>
                        <p>WP Site Status is a WordPress management dashboard that will help you to keep track of your sites and plugins and notify you when something needs to be updated. The dashboard will give you an overview of your sites and tell you if there are any major or minor updates available. It's a simple way to make sure you've done everything to keep hackers out, keeping performance up and your clients happy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="container-fluid text-center colorscheme--blue">
        <div class="container">
            <h2 class="mb-5">Benefits of WP Site Status</h2>
            <div class="row">
                <div class="col-12 col-lg-4 mb-5">
                    <h3>Security and performance</h3>
                    <p>With WP Site Status as your WordPress management dashboard we will help you to keep your sites up to date and make sure they have the latest fixes and features, the best performance and security fixes</p>
                </div>
                <div class="col-12 col-lg-4 mb-5">
                    <h3>Keep track of your sites and plugins</h3>
                    <p>WP Site Status will notify you when something needs updating, making sure you site will be update before anything breaks.</p>
                </div>
                <div class="col-12 col-lg-4 mb-5">
                    <h3>Happy clients</h3>
                    <p>By using WP Site Status and updating your site regularly will make your clients have the best experience and appreciate you more.</p>
                </div>
            </div>
            <a class="link-button" href="/why-wp-site-status">Read more about the benefits of an updated site</a>
        </div>
    </section>

    <section class="container-fluid supported-by colorscheme--green">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-8 text-center">
                    <h2 class="mb-5">Supported by</h2>
                    <a href="https://landsbygdsbyran.se" target="_blank">
                        <img src="/images/landsbygdsbyran.png" alt="Landsbygdsbyrån logo">
                    </a>
                </div>
            </div>
        </div>
    </section>
@stop
