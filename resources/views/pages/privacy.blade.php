@extends('layout')

@section('title', 'Privacy')

@section('container')
<div class="row legible">
    <div class="col-12 col-lg-7">
            <h1>Privacy Policy</h1>
            <p>WP Site Status operates wpsitestatus.io, which provides the SERVICE.</p>
            <p>This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service, the WP Site Status website.</p>
            <p>If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>

            <h2>Cookies</h2>
            <p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer’s hard drive.</p>
            <p>Our website only uses authentication cookies for users who are signed in to the Service.</p>

            <h2>Service Providers</h2>
            <p>We may employ third-party companies to facilitate our Service. We want to inform our Service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>
            <ul>
                <li>Digital Ocean - Hosting</li>
                <li>Sentry - Application monitoring platformg</li>
                <li>SendInBlue - Transactional email proxy</li>
            </ul>

            <h2>Security</h2>
            <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>

            <h2>User information</h2>
            <p>When you register an account we store some information about for the Service to work. The information stored about you is:</p>
            <ul>
                <li>Email</li>
                <li>Password</li>
            </ul>

            <p>You are responsible for keeping your passwords secure, and you agree not to disclose your passwords to any third party. You can delete your account and information at any time.</p>

            <p>The information saved about your sites is:</p>
            <ul>
                <li>Site name</li>
                <li>Site slug</li>
                <li>URL</li>
                <li>WordPress version</li>
                <li>PHP version</li>
                <li>If it's a multisite installation</li>
                <li>If the sites is the main site (in a multisite installation)</li>
            </ul>

            <p>The information saved about your plugins is:</p>
            <ul>
                <li>Plugin name</li>
                <li>Plugin slug</li>
                <li>Plugin version</li>
                <li>If it's active</li>
                <li>If it's active in a multisite installation</li>
            </ul>

            <h2>Changes to This Privacy Policy</h2>
            <p>We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page.</p>

            <h2>Contact Us</h2>
            <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.</p>
    </div>
    <div class="col-12 col-lg-5">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row align-items-center">
                    <div>
                        <img style="max-width:100px" src="/images/davidajnered.jpg" class="rounded-circle mr-3" alt="Founder and feveloper David Ajnered">
                    </div>
                    <div>
                        <b>Data Protection Officer</b><br>
                        David Ajnered<br>
                        <a href="mailto:support@wpsitestatus.io">support@wpsitestatus.io</a><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
