@extends('layout')

@section('title', 'About')

@section('container')
    <div class="row legible">
        <div class="col-12 col-lg-7">
            <h1>About the project</h1>
            <p>WP Site Status is a project by David Ajnered, a web developer located in Röstånga, Sweden.
                Work both as an employee at agencies and product companies as well as freelance and idealistic work.

            </p>
            <p>
                The idea for WP Site Status emerged at a previous employee a few years ago when David was tasked to develop and manage a large amount of WordPress sites.
                While the company closed down and the need disappeard WP Site Status was still developed just for the fun of it.
            </p>
            <p>
                In 2018 the project was rediscovered, and since last time the need had changed and a solution like WP Site Status would solve some every day problems for David.
                The project was updated and polished and developed so that it could be used by other developers or project managers as well, and in late 2019 WP Site Status was released.
            </p>
        </div>
        <div class="col-12 col-lg-5">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row align-items-center">
                        <div>
                            <img style="max-width:100px" src="/images/davidajnered.jpg" class="rounded-circle mr-3" alt="Founder and feveloper David Ajnered">
                        </div>
                        <div>
                            <b>Founder and developer</b><br>
                            David Ajnered<br>
                            <a href="mailto:support@wpsitestatus.io">support@wpsitestatus.io</a><br>
                            <a href="https://github.com/davidajnered">GitHub</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
