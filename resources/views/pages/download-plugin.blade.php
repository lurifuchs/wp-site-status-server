@extends('layout')

@section('title', 'Download WordPress plugin')

@section('container')
    <h1>Download WordPress plugin</h1>
    <div class="row legible">
        <div class="col-12 col-lg-8">
            @foreach ($releases as $release)
                <div class="card mb-3">
                    <div class="card-body">
                        <h2 class="my-0">{{ $release->name }}</h2>
                        <div class="text-muted">Released {{ date('Y-m-d', strtotime($release->released_at)) }}</div>
                        <p class="mt-3">{{ $release->description }}</p>
                        @foreach ($release->assets->sources as $asset)
                            <a class="btn btn-success btn-sm" href="{{ $asset->url }}">{{ $asset->format }} <i class="fa fa-download"></i></a>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection