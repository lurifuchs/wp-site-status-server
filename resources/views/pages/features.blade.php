@extends('layout')

@section('title', 'Features')

@section('container')
    <div class="row legible">
        <div class="col-12 col-lg-7">
            <h1>Features</h1>
            <p>WP Site Status aims at being a simple and easy-to-use tool for site owners, developers and project manager.</p>

            <h2>Notifications</h2>
            <p>Once a week we will send you an email with information about sites that needs updating.</p>

            <h2>Database Backups</h2>
            <p>Let wpsitestatus.io backup your database daily, weekly or monthly and have your database dump uploaded to your Dropbox. Other cloud storage providers will be available later. If you have a request for a specific storage, send us a mail to let us know which one.</p>

            <h2>Multisite support</h2>
            <p>Both notifications and database backups works with multisite installations of WordPress.</p>

            <p>Do you have a request for a feature? Send an email to <a href="mailto:support@wpsitestatus.io">support@wpsitestatus.io</a>, we'd love to hear your opinions.</p>

            <h2>Wishlist</h2>
            <ul>
                <li>More storage providers (AWS, Google Drive)</li>
                <li>Show uptime status.</li>
                <li>Integrate analytics and show an overview.</li>
            </ul>

        </div>
    </div>
@endsection
