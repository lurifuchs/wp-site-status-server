@extends('layout')

@section('title', 'Why do I need WP Site Status?')

@section('container')
    <div class="row legible">
        <div class="col-12 col-lg-7">
            <h1>Why do I need WP Site Status?</h1>
            <p>WordPress and it's eco system with plugins are open source which means they're constantly being developed and in the need of updating. It's usually you as a developer or project manager that has the responsibility to make sure your projects are up to date for a few reasons.</p>

            <h2>Security</h2>
            <p>Hackers are always trying to come up with new ways to weasel their way into our sites so they can steal data or plant malicious code. The developers of WordPress work hard to stay out in front of the hackers - patching known holes, and doing everything they can to prevent future vulnerabilities. But the only way for your site to benefit from these efforts is to stay updated.</p>

            <h2>Features and bugs</h2>
            <p>New versions of WordPress aren't just about keeping bad guys out. They are also about making it easier and more enjoyable for you to use, and about giving your client and their customers a better experience. If you stay on an old version of WordPress, you miss out on these new features.</p>

            <h2>Performance</h2>
            <p>A better site performance leads to a better experience for your users, which leads to more conversions, repeat visitors, and better search rankings, all of which increases your opportunity to build your audience and, ultimately, build a business around it.</p>

            {{-- <h2>But why doesn't WP Site Status do x, y and z?</h2>
            <p>There are other services doing similar things, and more</p>

            <h3>Backups</h3>
            <p>Store of site. Automated by hosting or by script</p>

            <h3>Remote updating</h3>
            <p>Not a good workflow. You probably have your code in a git repository. Recommended to use wp-cli and commit the updates to your repository</p>

            <h3>Malware scans</h3>
            <p>If your code is under version control the only place left to have undetected malware is in the uploads folder. By keeping WordPress and plugins updated you minimize the risk of security holes.</p> --}}
        </div>
    </div>
@endsection
