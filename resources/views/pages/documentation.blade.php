@extends('layout')

@section('title', 'Documentation')

@section('container')
    <div class="row legible">
        <div class="col-12 col-lg-6">
            <h1>How it works</h1>
            <p>To use WP Site Status you need an account and at least one WordPress site with the WP Site Status Client plugin installed. When correctly setup wpsitestatus.io will check the status of you sites once a week and check it there's any updates needed.</p>
        </div>

        <div class="col-12 col-lg-6">
            <div class="card">
                <div class="card-body">
                    <ol>
                        <li><a href="/register">Create an account</a> on wpsitestatus.io.</li>
                        <li>Download WP Site Status Client plugin from the <a href="/download-plugin">download page.</a></li>
                        <li>Install and activate the plugin on all sites you want to connect.</li>
                        <li>Copy the client token from your account page.</li>
                        <li>Go to Admin &rsaquo; Settings &rsaquo; WP Site Status Client and add the token.</li>
                        <li>Done! You can now view your site status on wpsitestatus.io.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
