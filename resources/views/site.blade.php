@extends('layout')

@section('title', $site->name)

@section('container')
    <h1>{{ $site->name }}</h1>
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card mb-5">
                <div class="card-body">
                    @if (!$site->getPlugins()->isEmpty())
                        @foreach ($site->getPlugins() as $plugin)
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">{{ $plugin->name }} </span>
                                <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">{{ $plugin->name }}</span>
                                <div class="badge badge-{{ $plugin->getStatusAsString($site->id) }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name }} - {{ $plugin->name }}</small>">
                                    {{ $plugin->getVersion($site->id) }}
                                    @if ($plugin->latest_version)
                                        <i class="fa fa-arrow-right"></i> {{ $plugin->latest_version }}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @else
                        <i>This site has no plugins.</i>
                    @endif
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <a href="/dashboard/site/{{ $site->slug }}/refresh" class="btn btn-success">Update sites and plugins</a>
                    <a href="/dashboard/site/{{ $site->slug }}/delete" class="btn btn-danger">Delete site</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card mb-5">
                <div class="card-header">Backup settings</div>
                <div class="card-body">
                    <form action="/dashboard/site/{{ $site->slug }}/update" method="post">
                        @csrf
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="backup_interval" id="backup_monthly" value="monthly" {{ $site->backup_interval === 'montly' ? ' checked' : '' }}>
                            <label class="form-check-label" for="backup_monthly">
                                Monthly
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="backup_interval" id="backup_weekly" value="weekly" {{ $site->backup_interval === 'weekly' ? ' checked' : '' }}>
                            <label class="form-check-label" for="backup_weekly">
                                Weekly
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="backup_interval" id="backup_daily" value="daily" {{ $site->backup_interval === 'daily' ? ' checked' : '' }}>
                            <label class="form-check-label" for="backup_daily">
                                Daily
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="backup_interval" id="backup_disabled" value="" {{ !$site->backup_interval ? ' checked' : '' }}>
                            <label class="form-check-label" for="backup_disabled">
                                Disabled
                            </label>
                        </div>

                        @if (!$site->backupStatuses()->isEmpty())
                            <div class="small mt-3">
                                @if ($site->backupStatuses()->first()->status === 'Success')
                                    Last backup was created at {{ date('Y-m-d', strtotime($site->backupStatuses()->first()->created_at)) }}
                                @else
                                    Backup was attempted at {{ date('Y-m-d', strtotime($site->backupStatuses()->first()->created_at)) }}
                                    <div class="alert alert-warning mt-3">{{ $site->backupStatuses()->first()->status }}</div>
                                @endif
                            </div>
                        @endif

                        <button type="submit" class="btn btn-success mt-3">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
