@extends('layout')

@section('title', '500')

@section('container')
    <h1>500</h1>
    <p>Something is broken, please try again or contact support@wpsitestatus.io.</p>
@endsection
