@extends('layout')

@section('title', '404')

@section('container')
    <h1>404</h1>
    <p>Page could not be found.</p>
@endsection
