@component('mail::message')
Here's you {{ $userData['notification_interval'] }} site and plugin status.
@foreach($userData['sites'] as $site)
<table width="100%">
<thead style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box;">
<tr>
<th width="70%" style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; border-bottom: 1px solid #edeff2; padding-bottom: 8px; margin: 0; text-align: left;">{{ $site['name'] }}</th>
<th width="30%" style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; border-bottom: 1px solid #edeff2; padding-bottom: 8px; margin: 0; text-align: left;"></th>
</tr>
</thead>
<tbody style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box;">
@foreach($site['plugins'] as $plugin)
<tr>
<td style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #74787e; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0; text-align: left;">{{ $plugin['name'] }}</td>
<td style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #74787e; font-size: 15px; line-height: 18px; padding: 10px 0; margin: 0; text-align: left;">
@if ($plugin['status'] === 'major')
<div style="background-color: #dc3545; display: inline-block; padding: .25em .4em; font-size: 75%; font-weight: 700; line-height: 1; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25rem; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #fff; font-size: 12px;">
{{ $plugin['version'] }} -> {{ $plugin['latest_version'] }}
</div>
@else
<div style="background-color: #f0b400; display: inline-block; padding: .25em .4em; font-size: 75%; font-weight: 700; line-height: 1; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25rem; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #fff; font-size: 12px;">
{{ $plugin['version'] }} -> {{ $plugin['latest_version'] }}
</div>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>
@endforeach

@component('mail::button', ['url' => 'https://wpsitestatus.io/account', 'color' => 'primary'])
Go to your account
@endcomponent
If you no longer want to receive these emails you need to change your email notification settings.
@endcomponent
