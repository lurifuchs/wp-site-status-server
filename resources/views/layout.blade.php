<!doctype html>
<html lang="en">
<head>
    <title>WP Site Status - @yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>
    <link rel="shortcut icon" type="image/png" href="https://wpsitestatus.io/images/favicon.png"/>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="description" content="WP Site Status is a WordPress management dashboard that will help you to keep track of your sites and plugins and notify you when something needs to be updated.">
    <meta name="keywords" content="Wordpress management dashboard,site management,wp multisite,wp dashboard,plugin manager,plugin management,wordpress update,wp update,update plugins,developer tool">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Vollkorn:600,700|Open+Sans:400,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="page-wrapper">
        <nav class="navbar navbar-expand-lg main-nav">
            <div class="container">
                <a class="navbar-brand header-4 header-bold" href="/">WP Site Status</a>
                <i class="navbar-toggler fa fa-bars collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"></i>
                @include('components.desktop-nav')
                @include('components.mobile-nav')
            </div>
        </nav>

        @include('components.alert')

        @hasSection('container-fluid')
            <main>
                @yield('container-fluid')
            </main>
        @endif

        @hasSection('container')
            <main class="container mt-5 mb-5">
                <div class="row">
                    <div class="col-12">
                        @yield('container')
                    </div>
                </div>
            </main>
        @endif
    </div>

    <footer class="container-fluid">
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col-12 col-lg-4 mb-5">
                    <h4>WP Site Status</h4>
                    <ul>
                        <li><a href="/features">Features</a></li>
                        {{-- <li><a href="/about">About</a></li> --}}
                        <li><a href="/documentation">Documentation</a></li>
                        <li><a href="/terms">Terms and condition</a></li>
                        <li><a href="/privacy">Privacy policy</a></li>
                        <li><a href="/download-plugin">Download WordPress plugin</a></li>
                    </ul>
                </div>
                <div class="col-12 col-lg-4 mb-5">
                    <h4>About</h4>
                    WP Site Status is a management tool that will help you to keep track of your sites and plugins. It's a simple way to keep hackers out, keeping performance up and your clients happy.
                </div>
                <div class="col-12 col-lg-4">
                    <h4>Contact</h4>
                    <ul>
                        <li>support@wpsitestatus.io</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $('#navbar').on('shown.bs.collapse', function() {
                $('html').addClass('no-scroll');
            });
            $('#navbar').on('hidden.bs.collapse', function() {
                $('html').removeClass('no-scroll');
            });
        })
    </script>
</body>
</html>
