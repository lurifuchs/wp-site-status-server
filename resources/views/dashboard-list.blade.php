<div class="container dashboard-list">
    <div class="row">
        <div class="card-columns">
            @foreach ($filteredSites as $site)
                <div class="card mb-4">
                    <div class="card-header site-header d-flex justify-content-between">
                        <span>
                            @if ($site->main_site)
                                <span class="fa fa-globe text-success mr-1" title="Main site"></span>
                            @endif
                            {{ $site->name }}
                        </span>
                        <div>
                            <a href="/dashboard/site/{{ $site->slug }}" title="Settings"><i class="ml-3 fa fa-cog"></i></a>
                            <a href="{{ $site->url }}" target="_blank" title="Visit site"><i class="ml-1 fas fa-external-link-alt"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">PHP</span>
                            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">PHP</span>
                            <span class="badge badge-{{ $site->getPhpStatusAsString() }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name}} - PHP</small>">{{ $site->php_version }}</span>
                        </div>

                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">WordPress</span>
                            <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">WordPress</span>
                            <span class="badge badge-{{ $site->getWpStatusAsString() }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name }} - WordPress</small>">{{ $site->version }}</span>
                        </div>

                        @if (!$site->getPlugins()->isEmpty())
                            @foreach ($site->getPlugins() as $plugin)
                                <div class="d-flex justify-content-between align-items-center mb-2">
                                    <span class="d-none d-sm-block d-md-block d-lg-none d-xl-block">{{ $plugin->name }} </span>
                                    <span class="d-block d-sm-none d-md-none d-lg-block d-xl-none">{{ $plugin->name }}</span>
                                    @include('components/plugin-status-badge')
                                </div>
                            @endforeach
                        @else
                            <div class="mt-5">
                                <i>This site has no plugins.</i>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
