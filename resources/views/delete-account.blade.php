@extends('layout')

@section('title', 'Delete account')

@section('container')
    <h1>Confirm account deletion</h1>
    <div class="card mb-5">
        <div class="card-body">
            <p>Sorry to see you go. When you delete your account the information about your sites and plugins will be deleted. If you have any questions please contact <a href="mailto:support@wpsitestatus.io">support@wpsitestatus.io</a>.</p>
        </div>
        <div class="card-footer d-flex justify-content-between">
            <a href="/account" class="btn btn-success">&laquo; Go back</a>
            <a href="/account/delete/confirm" class="btn btn-danger">Delete account</a>
        </div>
    </div>
@endsection
