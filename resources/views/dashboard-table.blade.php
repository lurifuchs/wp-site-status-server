<div class="container-fluid dashboard-table">
    <div class="table-scroll mb-5">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th class="plugin-header">Plugin</th>
                @foreach ($filteredSites as $site)
                    <th>
                        <div class="site-header">
                            {{ $site->name }}
                            <a href="/dashboard/site/{{ $site->slug }}"><i class="ml-3 fa fa-cog"></i></a>
                            <a href="{{ $site->url }}" target="_blank"><i class="ml-1 fas fa-external-link-alt"></i></a>
                        </div>
                    </th>
                @endforeach
            </tr>

            <tr>
                <td class="plugin-header">PHP</td>
                @foreach ($filteredSites as $site)
                    <td><span class="badge badge-{{ $site->getPhpStatusAsString() }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name}} - PHP</small>">{{ $site->php_version }}</span></td>
                @endforeach
            </tr>

            <tr>
                <td class="plugin-header">WordPress</td>
                @foreach ($filteredSites as $site)
                    <td><span class="badge badge-{{ $site->getWpStatusAsString() }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>{{ $site->name }} - WordPress</small>">{{ $site->version }}</span></td>
                @endforeach
            </tr>

            @if (!$filteredPlugins->isEmpty())
                @foreach ($filteredPlugins as $plugin)
                    <tr>
                        <td class="plugin-header">
                            {{ $plugin->name }}
                            @if (!$plugin->latest_version && $plugin->processed)
                                <i class="fa fa-times-circle ml-1" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>Unable to find version information</small>"></i>
                            @elseif (!$plugin->latest_version)
                                <i class="fa fa-exclamation-circle ml-1" data-toggle="tooltip" data-placement="top" data-html="true" title="<small>We are working on finding the latest version for this plugin</small>"></i>
                            @endif
                        </td>

                        @foreach ($filteredSites as $site)
                            <td class="plugin">
                                @if ($plugin->getVersion($site->id))
                                    @include('components/plugin-status-badge')
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @else
                No plugins installed
            @endif
        </table>
    </div>
</div>
