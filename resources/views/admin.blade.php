@extends('layout')

@section('title', 'Admin')

@section('container')
    <div class="card">
        <div class="card-header">Reported plugins</div>
        @if (!$plugins->isEmpty())
            <form action="/admin/plugins" method="POST">
                <div class="card-body">
                    @csrf
                    @foreach ($plugins as $plugin)
                        <div>
                            <div class="form-group">
                                <label for="{{ $plugin->slug }}-slug">{{ $plugin->name }}</label>
                                <input type="text" class="form-control" id="{{ $plugin->id }}-slug" name="{{ $plugin->id }}[slug]" placeholder="{{ $plugin->name }}" value="{{ old('email', $plugin->slug) }}">
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="{{ $plugin->id }}-processed" name="{{ $plugin->id }}[processed]" value="true">
                                    <label class="form-check-label" for="{{ $plugin->slug }}-processed">Processed</label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        @else
            <div class="card-body">
                All plugins are processed
            </div>
        @endif
    </div>
@stop
