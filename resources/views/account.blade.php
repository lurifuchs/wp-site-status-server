@extends('layout')

@section('title', 'Account')

@section('container')
    <h1>Account</h1>
    <form action="/account/update" method="POST" class="card mb-5">
        @csrf
        <div class="card-body">
            <div class="form-group{{ $errors->has('email') ? ' error' : '' }}">
                <label for="email">Email address</label>
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" placeholder="Your email address" value="{{ old('email', $user->email) }}" required>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' error' : '' }}">
                <label for="password">Password</label>
                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Change your password">
            </div>

            <div class="form-group">
                <label for="email">Client token</label>
                <input type="text" class="form-control" id="text" name="text" placeholder="Token is missing, please contact support" value="{{ $user->token }}" readonly>
            </div>

            <div class="form-group">
                <label>Notification emails</label>
                <div class="form-check">
                    <input class="form-check-input" name="notification_interval" type="radio" value="daily" id="daily" {{ old('notifications', $user->notification_interval) === 'daily' ? 'checked' : ''}}>
                    <label class="form-check-label" for="daily">
                        Daily
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="notification_interval" type="radio" value="weekly" id="weekly" {{ old('notifications', $user->notification_interval) === 'weekly' ? 'checked' : ''}}>
                    <label class="form-check-label" for="weekly">
                        Weekly
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="notification_interval" type="radio" value="monthly" id="monthly" {{ old('notifications', $user->notification_interval) === 'monthly' ? 'checked' : ''}}>
                    <label class="form-check-label" for="monthly">
                        Monthly
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="notification_interval" type="radio" value="disabled" id="disabled" {{ old('notifications', $user->notification_interval) === 'disabled' ? 'checked' : ''}}>
                    <label class="form-check-label" for="disabled">
                        Disabled
                    </label>
                </div>
                <small>
                    Notifications will only be sent if there is anything to update.<br>
                    @if ($user->notification_interval !== 'disabled')
                        Next notification will be sent on {{ substr($user->next_notification_date, 0, 10) }}.
                    @endif
                </small>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-success" type="submit">Update your account information</button>
        </div>
    </form>

    @if ($user->logs->count() > 0)
        <h2>Logs</h2>
        <div class="card mb-3">
            <table class="logs">
                @foreach ($user->logs as $log)
                    <tr>
                        <td class="log__date">{{ $log->created_at }}</td>
                        <td class="log__status">{{ $log->status }}</td>
                        <td class="log__referer">{{ $log->referer }}</td>
                        <td class="log__log">{{ $log->log }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endif

    <a href="/account/delete" class="btn btn-danger mt-5">Delete your account</a>
@stop
