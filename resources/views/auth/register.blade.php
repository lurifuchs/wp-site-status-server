@extends('layout')

@section('title', 'Register')

@section('container')
    <div class="col-12 col-lg-6 offset-lg-3">
        <h1>Create account</h1>
        <div class="card">
            <div class="card-body">
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="form-group{{ $errors->has('email') ? ' error' : '' }}">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" placeholder="Email" value="{{ old('email', '') }}">
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' error' : '' }}">
                        <label for="password">Password</label>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Password" value="{{ old('password', '') }}">
                    </div>

                    <button type="submit" class="btn btn-success">Create your account</button>
                    <small class="form-text text-muted mt-2">By signing up you agree to our <a href="">terms of service</a> and <a href="">privacy policy</a>.</small>
                </form>
            </div>
        </div>
    </div>
@endsection
