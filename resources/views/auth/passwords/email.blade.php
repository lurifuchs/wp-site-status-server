@extends('layout')

@section('title', 'Reset password')

@section('container')
    <div class="col-12 col-lg-6 offset-lg-3">
        <h1>Reset password</h1>
        <div class="card">
            <div class="card-body">
                <form action="{{ route('password.email') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email" class="">E-mail address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Your email address" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">Send password reset link</button>
                </form>
            </div>
        </div>
    </div>
@endsection
