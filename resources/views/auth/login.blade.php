@extends('layout')

@section('title', 'Login')

@section('container')
    <div class="col-12 col-lg-6 offset-lg-3">
        <h1>Log in</h1>
        <div class="card">
            <div class="card-body">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" placeholder="Email" value="{{ old('email', '') }}">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Password" value="{{ old('password', '') }}">
                    </div>

                    <button type="submit" class="btn btn-success">Log in to your account</button>
                    <a class="form-text mt-2" href="/password/reset">Forgot your password?</a>
                </form>
            </div>
        </div>
    </div>
@endsection
