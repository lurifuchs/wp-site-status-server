@extends('layout')

@section('title', 'Dashboard')

@section('container-fluid')
    <div class="dashboard">
        @if ($sites->isEmpty())
            <div class="container mt-5 mb-5">
                <h1>Your dashboard is very empty...</h1>
                <div class="card">
                    <div class="card-body legible">
                        <ol>
                            <li>Download the WP Site Status Client plugin from <a href="/download-plugin">the download page.</a></li>
                            <li>Install and activate the plugin on all sites you want to connect.</li>
                            <li>Copy the client token from your account page.</li>
                            <li>Navigate to Admin &rsaquo; Settings &rsaquo; WP Site Status Client and add the token.</li>
                            <li>Your site will automatically connect with wpsitestatus.io and setup is complete!</li>
                        </ol>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row filters mb-3 mt-5">
                    <form id="filter-form" class="form-inline" action="/dashboard" method="get">
                        <div class="form-group">
                            <select class="form-control" id="site-filter" name="site">
                                <option value="">{{ Request::get('site') ? 'Show all sites' : 'Filter sites' }}</option>
                                @foreach ($filter as $site)
                                    <option value="{{ $site->slug }}" {{ $site->slug == Request::get('site') ? 'selected' : '' }}>{{ $site->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <a href="/dashboard/list" class="btn btn-secondary ml-3"><i class="fas fa-grip-vertical"></i></a>
                        <a href="/dashboard/table" class="btn btn-secondary ml-2"><i class="fa fa-list-ul"></i></a>
                    </form>
                </div>

                <div class="row explanations mb-4">
                    <span class="badge badge-success">Up to date</span>
                    <span class="badge badge-success"><span class="fa fa-globe" aria-hidden="true"></span> Multisite</span>
                    <span class="badge badge-danger">Major </span>
                    <span class="badge badge-warning">Minor</span>
                    <span class="badge badge-secondary">No info</span>
                    <span class="badge badge-secondary inactive">Inactive</span>
                </div>
            </div>

            @if ($user->dashboard_type === 'table')
                @include('dashboard-table')
            @else
                @include('dashboard-list')
            @endif
        @endif
    </div>

    <script>
        document.getElementById('site-filter').addEventListener('change', function() {
            document.getElementById('filter-form').submit();
        });
    </script>
@stop
