<?php

// Public
Route::get('/', 'PageController@index');
Route::get('/features', 'PageController@features');
Route::get('/why-wp-site-status', 'PageController@why');
// Route::get('/about', 'PageController@about');
Route::get('/terms', 'PageController@terms');
Route::get('/privacy', 'PageController@privacy');
Route::get('/documentation', 'PageController@documentation');
Route::get('/download-plugin', 'PageController@downloadPlugin');

// Plugin callback
Route::post('/api/client', 'ApiController@client');
Route::post('/api/log', 'ApiController@log');

// Protected routes
Route::get('/dashboard/{type?}', 'AccountController@dashboard')->middleware('auth');
Route::get('/dashboard/site/{siteSlug}', 'AccountController@site')->middleware('auth');
Route::get('/dashboard/site/{siteSlug}/refresh', 'AccountController@refreshSiteData')->middleware('auth');
Route::get('/dashboard/site/{siteSlug}/delete', 'AccountController@deleteSite')->middleware('auth');
Route::post('/dashboard/site/{siteSlug}/update', 'AccountController@updateSite')->middleware('auth');
Route::post('/dashboard/plugin/{pluginId}/report', 'AccountController@reportPlugin')->middleware('auth');

Route::get('/account', 'AccountController@account')->middleware('auth');
Route::post('/account/update', 'AccountController@updateAccount')->middleware('auth');
Route::get('/account/delete', 'AccountController@deleteAccount')->middleware('auth');
Route::get('/account/delete/confirm', 'AccountController@deleteAccountConfirm')->middleware('auth');

// Auth routes
Auth::routes();
Route::get('/logout', 'AccountController@logout');

// Admin
Route::get('/admin', 'AdminController@admin')->middleware('auth');
Route::post('/admin/plugins', 'AdminController@plugins')->middleware('auth');

// System
Route::get('/system/cron/collect', 'CronController@collect');
Route::get('/system/cron/notifications', 'CronController@notifications');
Route::get('/system/cron/backups', 'CronController@backups');
Route::get('/system/sitemap', 'SystemController@sitemap');
Route::get('/system/user/settings', 'SystemController@userSettings');
