# Call to client
* debug parameter - print curl result. Helpful for getting Server error response printed directly.
* dev parameter - Allow self signed certificate

# Cleanup map table
Not sure where to implement yet

```
$dirtyRows = DB::table('site_plugins')
->leftJoin('sites', 'sites.id', '=', 'site_plugins.site_id')
->leftJoin('plugins', 'plugins.id', '=', 'site_plugins.plugin_id')
->where(function($query) {
    $query->whereNull('sites.id')->orWhereNull('plugins.id');
})->delete();
```

ngrok http -region=eu -host-header=rewrite -hostname=webhooks.wpsitestatus.io wpsitestatus.test:443

# Use plugin dev mode
Add `wp_site_status_client_dev_mode` option to site.
ngrok http -region=eu -host-header=rewrite -hostname=dev.wpsitestatus.io wpsitestatus.test:443

centralized WordPress management dashboard