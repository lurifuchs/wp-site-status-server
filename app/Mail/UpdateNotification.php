<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user, sites and plugin data.
     *
     * @var array
     */
    public $userData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userData)
    {
        $this->userData = $userData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from([
            'address' => 'no-reply@wpsitestatus.io',
            'name' => 'WP Site Status'
        ])
        ->subject('Site and plugin status - ' . date('Y-m-d'))
        ->markdown('mail.update-notification');
    }
}
