<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use Spatie\Sitemap\SitemapGenerator;

class SystemController extends BaseController
{
    public function sitemap()
    {
        SitemapGenerator::create(env('APP_URL'))->writeToFile(env('APP_SITEMAP'));
    }

    /**
     * Store user cookie settings in session.
     *
     * @param Request $request
     * @return void
     */
    protected function userSettings(Request $request)
    {
        if ($request->input('cookies') === 'accept') {
            $request->session()->put('cookies', 'accept');
        } else {
            $request->session()->put('cookies', 'reject');
        }

        return back();
    }
}