<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

use App\Variable;
use App\Mail\UpdateNotification;
use App\User;
use App\Site;
use App\BackupStatus;

class CronController extends BaseController
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Collect site data.
     *
     * @return void
     */
    public function collect()
    {
        $this->getWPCoreVersion();
        $this->getPHPVersion();
        $this->getSiteData();
    }

    /**
     * Send notifications
     *
     * @return void
     */
    public function notifications()
    {
        $this->sendNotifications();
    }

    /**
     * Backup.
     *
     * @return void
     */
    public function backups()
    {
        $response = $this->triggerBackups();
        return response()->json($response);
    }

    /**
     * Get current WordPress core version.
     *
     * @return void
     */
    private function getWPCoreVersion()
    {
        $response = $this->client->get('https://api.wordpress.org/core/version-check/1.7/');
        $response = json_decode($response->getBody());
        $wpCoreVersion = $response->offers[0]->version;

        $variable = Variable::where('key', 'wp_core_version')->first();
        if ($variable) {
            $variable->value = $wpCoreVersion;
            $variable->save();
        } else {
            Variable::create([
                'key' => 'wp_core_version',
                'value' => $wpCoreVersion
            ]);
        }
    }

    /**
     * Get latest PHP version.
     *
     * @return void
     */
    private function getPHPVersion()
    {
        $response = $this->client->get('https://php.net/releases/index.php?json');
        $response = json_decode($response->getBody());
        $phpVersion = $response->{max(array_keys((array) $response))}->version;

        $variable = Variable::where('key', 'php_version')->first();
        if ($variable) {
            $variable->value = $phpVersion;
            $variable->save();
        } else {
            Variable::create([
                'key' => 'php_version',
                'value' => $phpVersion
            ]);
        }
    }

    /**
     * Trigger sites to update data.
     *
     * @return void
     */
    private function getSiteData()
    {
        $sites = Site::all();
        $sites->each(function($site) {
            try {
                $this->client->get($site->getUpdateUrl());
            } catch(\Exception $e) {
                \Log::error('Site ' . $site->name . 'could not be updated.');
            }
        });
    }

    /**
     * Send email notifications.
     *
     * @return void
     */
    private function sendNotifications()
    {
        $results = DB::table('site_plugins')
        ->join('sites', 'sites.id', '=', 'site_plugins.site_id')
        ->join('plugins', 'plugins.id', '=', 'site_plugins.plugin_id')
        ->join('users', 'users.id', '=', 'sites.user_id')
        ->select(
            'site_plugins.version AS site_plugin_version',
            'plugins.id AS plugin_id',
            'plugins.name AS plugin_name',
            'plugins.slug AS plugin_slug',
            'plugins.latest_version AS plugin_latest_version',
            'sites.id AS site_id',
            'sites.name AS site_name',
            'sites.slug AS site_slug',
            'sites.url AS site_url',
            'sites.version AS site_version',
            'sites.php_version AS site_php_version',
            'users.id AS user_id',
            'users.email AS user_email',
            'users.notification_interval AS user_notification_interval',
            DB::raw("
                CASE WHEN (
                    LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(site_plugins.version, '.', 1), '.', -1), 10, '0')
                    <
                    LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(plugins.latest_version, '.', 1), '.', -1), 10, '0')
                ) THEN 'major' ELSE 'minor' END AS status
            ")
        )
        ->whereRaw("
            CONCAT(
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(site_plugins.version, '.', 1), '.', -1), 10, '0'),
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(site_plugins.version, '.', 2), '.', -1), 10, '0'),
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(site_plugins.version, '.', 3), '.', -1), 10, '0')
            )
            <
            CONCAT(
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(plugins.latest_version, '.', 1), '.', -1), 10, '0'),
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(plugins.latest_version, '.', 2), '.', -1), 10, '0'),
                LPAD(SUBSTRING_INDEX(SUBSTRING_INDEX(plugins.latest_version, '.', 3), '.', -1), 10, '0')
            )"
        )
        ->where('users.next_notification_date', '<=', date('Y-m-d H:i:s'))
        ->get();

        $users = [];
        if ($results) {
            foreach ($results as $row) {
                // Ignore users with disabled notifications
                if ($row->user_notification_interval === 'disabled') {
                    continue;
                }

                // User data
                if (!isset($users[$row->user_id])) {
                    $users[$row->user_id] = [
                        'email' => $row->user_email,
                        'notification_interval' => $row->user_notification_interval,
                    ];
                }

                // Site data
                if (!isset($users[$row->user_id]['sites'][$row->site_id])) {
                    $users[$row->user_id]['sites'][$row->site_id] = [
                        'id' => $row->site_id,
                        'name' => $row->site_name,
                        'slug' => $row->site_slug,
                        'url' => $row->site_url,
                        'version' => $row->site_version,
                        'php_version' => $row->site_php_version,
                    ];
                }

                // Plugin data
                $users[$row->user_id]['sites'][$row->site_id]['plugins'][$row->plugin_id] = [
                    'id' => $row->plugin_id,
                    'name' => $row->plugin_name,
                    'slug' => $row->plugin_slug,
                    'version' => $row->site_plugin_version,
                    'latest_version' => $row->plugin_latest_version,
                    'status' => $row->status,
                ];
            }
        }

        foreach ($users as $userId => $userData) {
            Mail::to($userData['email'])->send(new UpdateNotification($userData));

            // Update next notification date
            $user = User::find($userId);
            if ($userData['notification_interval'] === 'daily') {
                $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 day'));
            } else if ($userData['notification_interval'] === 'weekly') {
                $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 week'));
            } else {
                $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 month'));
            }
            $user->save();
        }
    }

    /**
     * Trigger backups and set new backup date.
     *
     * @return void
     */
    private function triggerBackups()
    {
        $sites = Site::where('sites.next_backup_date', '<=', date('Y-m-d H:i:s'))->get();
        $response = [];

        $sites->each(function($site) use (&$response) {
            $response[$site->id]['site'] = $site->name;

            $backupStatus = new BackupStatus();
            $backupStatus->site_id = $site->id;

            try {
                $this->client->get($site->getBackupUrl());

                if ($site->backup_interval === 'daily') {
                    $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 day'));
                } else if ($site->backup_interval === 'weekly') {
                    $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 week'));
                } else if ($site->backup_interval === 'monthly') {
                    $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 month'));
                }

                $site->save();

                $response[$site->id]['message'] = 'Success';
                $backupStatus->status = 'Success';
                $backupStatus->save();
            } catch(\Exception $e) {
                $response[$site->id]['message'] = $e->getMessage();
                $backupStatus->status = $e->getMessage();
                $backupStatus->save();
            }
        });

        return $response;
    }
}
