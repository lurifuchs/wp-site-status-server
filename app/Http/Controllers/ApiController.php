<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\User;
use App\Site;
use App\Plugin;
use App\SitePlugin;
use App\Log;

class ApiController extends BaseController
{
    /**
     * Action: Receive data from clients
     *
     * @param Request $request
     * @return void
     */
    protected function client(Request $request)
    {
        app('debugbar')->disable();

        $client = new Client();
        $user = User::where('token', $request->input('token'))->first();

        if (!$user) {
            return response()->json(['error' => 'No user with the provided token was found'], 404);
        }

        foreach ($request->input('sites') as $siteData) {
            $site = Site::where('url', '=', $siteData['url'])->first();

            // Create site if it doesn't exist
            if (!$site) {
                $site = new Site();
            }

            $site->user_id = $user->id;
            $site->name = $siteData['name'];
            $site->slug = $siteData['slug'];
            $site->url = $siteData['url'];
            $site->version = $siteData['version'];
            $site->php_version = $siteData['php_version'];
            $site->multisite = $siteData['multisite'];
            $site->main_site = $siteData['main_site'];
            $site->save();

            // Loop plugin data
            if (isset($siteData['plugins'])) {
                foreach ($siteData['plugins'] as $pluginData) {
                    $plugin = Plugin::where('name', '=', $pluginData['name'])->first();

                    if (!$plugin) {
                        $plugin = new Plugin();
                        $plugin->name = $pluginData['name'];
                        $plugin->slug = $pluginData['slug'];
                    }

                    $plugin->updateLatestVersion($client);
                    $plugin->save();

                    // Save site and plugin id to mapping table for easy selection and filtering
                    // If uniqie key (both id's) already exist, just ignore inserting a new row.
                    $sitePlugin = SitePlugin::where('site_id', '=', $site->id)->where('plugin_id', '=', $plugin->id)->first();

                    if (!$sitePlugin) {
                        $sitePlugin = new SitePlugin();
                        $sitePlugin->site_id = $site->id;
                        $sitePlugin->plugin_id = $plugin->id;
                    }

                    $sitePlugin->version = $pluginData['version'];

                    // Set activation status
                    $active = false;
                    $network_active = false;
                    if (isset($siteData['active_plugins'][$pluginData['path']])) {
                        // Active
                        if ($siteData['active_plugins'][$pluginData['path']]['active'] == true) {
                            $active = true;
                        }

                        // Network active
                        if ($siteData['active_plugins'][$pluginData['path']]['network_active'] == true) {
                            $network_active = true;
                        }
                    }

                    $sitePlugin->active = $active;
                    $sitePlugin->network_active = $network_active;

                    $sitePlugin->save();
                }
            }
        }

        return response()->json(['data' => 'Sync completed'], 200);
    }

    /**
     * Receive log data from client.
     *
     * @param Request $request
     * @return void
     */
    protected function log(Request $request)
    {
        app('debugbar')->disable();

        // Write response to log table
        $user = User::where('token', $request->input('token'))->first();

        if (!$user) {
            return response()->json(['error' => 'No user with the provided token was found'], 404);
        }

        $response = $request->input('response');
        Log::create([
            'user_id' => $user->id,
            'referer' => $request->input('referer'),
            'status' => $response['code'],
            'log' => $response['message'],
        ]);
    }
}
