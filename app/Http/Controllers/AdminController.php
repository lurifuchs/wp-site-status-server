<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use App\Plugin;

class AdminController extends BaseController
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = Auth::user();

            if (!(bool) $user->is_admin) {
                return redirect('/dashboard');
            }

            return $next($request);
        });
    }

    public function admin()
    {
        $user = Auth::user();
        $plugins = Plugin::whereNull('latest_version')->whereNull('processed')->get();

        return view('admin', [
            'user' => $user,
            'plugins' => $plugins
        ]);
    }

    public function plugins(Request $request)
    {
        $client = new Client();
        $plugins = Plugin::whereNull('latest_version')->whereNull('processed')->get();

        foreach ($plugins as $plugin) {
            if ($request->has($plugin->id)) {
                $pluginData = $request->input($plugin->id);

                $plugin->slug = $pluginData['slug'];

                if (isset($pluginData['processed'])) {
                    $plugin->processed = 1;
                }

                $plugin->updateLatestVersion($client, true);
                $plugin->save();
            }
        }

        return redirect()->back();
    }
}
