<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Client;

use Illuminate\Routing\Controller as BaseController;

class PageController extends BaseController
{
    /**
     * Action: Logout
     *
     * @param Request $request
     * @return void
     */
    protected function logout(Request $request)
    {
        Auth::logout();

        return redirect('/');
    }

    /**
     * View: Front page
     *
     * @param Request $request
     * @return void
     */
    protected function index(Request $request)
    {
        $user = Auth::user();

        return view('index', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: about
     *
     * @param Request $request
     * @return void
     */
    protected function about(Request $request)
    {
        return view('pages.about', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: features
     *
     * @param Request $request
     * @return void
     */
    protected function features(Request $request)
    {
        return view('pages.features', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: why
     *
     * @param Request $request
     * @return void
     */
    protected function why(Request $request)
    {
        return view('pages.why', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: terms
     *
     * @param Request $request
     * @return void
     */
    protected function terms(Request $request)
    {
        return view('pages.terms', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: privacy
     *
     * @param Request $request
     * @return void
     */
    protected function privacy(Request $request)
    {
        return view('pages.privacy', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * View: Documentation
     *
     * @param Request $request
     * @return void
     */
    protected function documentation(Request $request)
    {
        return view('pages.documentation', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * Download page.
     *
     * @return void
     */
    protected function downloadPlugin()
    {
        try {
            $client = new Client();
            $url = 'https://gitlab.com/api/v4/projects/' . env('GITLAB_PROJECT') . '/releases';
            $response = $client->request('GET', $url, [
                'headers' => [
                    'PRIVATE-TOKEN' => env('GITLAB_TOKEN')
                ]
            ]);
            $releases = json_decode($response->getBody());
        } catch (\Exception $e) {
            $releases = null;
        }

        return view('pages.download-plugin', [
            'user' => Auth::user(),
            'releases' => $releases,
        ]);
    }
}
