<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

use GuzzleHttp\Client;

use App\Site;
use App\Plugin;
use App\SitePlugin;

use Illuminate\Routing\Controller as BaseController;

class AccountController extends BaseController
{
    /**
     * Action: Logout
     *
     * @param Request $request
     * @return void
     */
    protected function logout(Request $request)
    {
        Auth::logout();

        return redirect('/');
    }

    /**
     * View: Account
     *
     * @param Request $request
     * @return void
     */
    protected function account(Request $request)
    {
        $user = Auth::user();

        if (!$user->token) {
            $user->token = Str::random(32);
            $user->save();
        }

        return view('account', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * Action: Update account
     *
     * @param Request $request
     * @return void
     */
    protected function updateAccount(Request $request)
    {
        $user = Auth::user();

        $validate = [
            'email' => $request->input('email')
        ];

        if ($request->input('password')) {
            $validate['password'] = $request->input('password');
        }

        $validator = Validator::make($validate, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
            'password' => ['string', 'min:8'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $user->email = $request->input('email');

            if ($request->input('password')) {
                $user->password = Hash::make($request->input('password'));
            }

            if ($request->input('notifications') !== $user->notification_interval) {
                $user->notification_interval = $request->input('notification_interval');

                if ($user->notification_interval === 'daily') {
                    $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 day'));
                } else if ($user->notification_interval === 'weekly') {
                    $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 week'));
                } else {
                    $user->next_notification_date = date('Y-m-d 00:00:00', strtotime('+1 month'));
                }
            }

            $user->save();

            return redirect()->back()->with('message', 'Your account information has been updated.');
        }
    }

    /**
     * View: Delete account
     *
     * @param Request $request
     * @return void
     */
    protected function deleteAccount(Request $request)
    {
        return view('delete-account', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * Action: delete account
     *
     * @param Request $request
     * @return void
     */
    protected function deleteAccountConfirm(Request $request)
    {
        $user = Auth::user();

        $user->delete();

        return redirect('/')->with('message', 'Your account has been deleted.');
    }

    /**
     * View: Dashboard
     *
     * @param Request $request
     * @return void
     */
    protected function dashboard(Request $request, $type = null)
    {
        $user = Auth::user();

        // Reset filter
        if ($request->has('site') && $request->input('site') == '') {
            return redirect('/dashboard');
        }

        if ($type) {
            $user->dashboard_type = $type === 'table' ? 'table' : 'list';
            $user->save();

            return redirect('/dashboard');
        }

        $sites = Site::where('user_id', $user->id)->get();
        $plugins = Plugin::all();
        $sitePlugins = SitePlugin::all();

        $filteredSites = new Collection();
        $filteredPluginIds = new Collection();

        // Build site and plugin relationship
        $sites->each(function($site) use ($plugins, $filteredSites) {
            $sitePlugins = SitePlugin::where('site_id', $site->id)->get();

            if (!$sitePlugins->isEmpty()) {
                $sitePlugins->each(function($sitePlugin) use ($site, $plugins, $filteredSites) {
                    $plugin = $plugins->where('id', $sitePlugin->plugin_id)->first();
                    $plugin->setVersion($site->id, $sitePlugin->version);
                    $plugin->setInstalledOnSite($site->id, $sitePlugin->active);
                    $plugin->setInstalledOnNetwork($site->id, $sitePlugin->network_active);
                    $site->setPlugins($plugin);
                    $filteredSites->put($site->id, $site);
                });
            } else {
                $filteredSites->put($site->id, $site);
            }
        });

        // Set filter
        $filter = $filteredSites;

        // Filter on specific site
        if ($request->has('site')) {
            $filteredSites = $filteredSites->where('slug', $request->input('site'));
        }

        // Filter plugins based on sites
        $filteredPluginIds = $filteredSites->map(function($site) {
            return $site->getPlugins()->map(function($plugin) {
                return $plugin->id;
            });
        })->flatten()->unique();
        $filteredPlugins = $plugins->whereIn('id', $filteredPluginIds);

        return view('dashboard', [
            'user' => $user,
            'sites' => $sites,
            'filter' => $filter,
            'filteredSites' => $filteredSites->sortBy('name'),
            'filteredPlugins' => $filteredPlugins->sortBy('name'),
        ]);
    }

    /**
     * View: Site
     *
     * @param Request $request
     * @param string $siteSlug
     * @return void
     */
    protected function site(Request $request, $siteSlug)
    {
        $user = Auth::user();

        try {
            $site = Site::where('slug', $siteSlug)->where('user_id', $user->id)->first();
            $sitePlugins = SitePlugin::where('site_id', $site->id)->get();

            // Build site and plugin relationship
            $sitePlugins->each(function($sitePlugin) use ($site) {
                $plugin = Plugin::where('id', $sitePlugin->plugin_id)->first();

                if ($plugin) {
                    $plugin->setVersion($site->id, $sitePlugin->version);
                    $plugin->setInstalledOnSite($site->id, $sitePlugin->active);
                    $plugin->setInstalledOnNetwork($site->id, $sitePlugin->network_active);

                    $site->setPlugins($plugin);
                }
            });
        } catch(\Exception $e) {
            return response()->view('errors.404', [
                'user' => $user,
            ], 404);
        }

        return view('site', [
            'user' => $user,
            'site' => $site,
        ]);
    }

    /**
     * Update site settings.
     *
     * @param Request $request
     * @param string $siteSlug
     * @return void
     */
    protected function updateSite(Request $request, $siteSlug)
    {
        $user = Auth::user();
        $site = Site::where('slug', $siteSlug)->where('user_id', $user->id)->first();

        if ($request->input('backup_interval') && $request->input('backup_interval') !== $site->backup_interval) {
            $site->backup_interval = $request->backup_interval;

            if ($site->backup_interval === 'daily') {
                $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 day'));
            } else if ($site->backup_interval === 'weekly') {
                $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 week'));
            } else if ($site->backup_interval === 'montly') {
                $site->next_backup_date = date('Y-m-d 00:00:00', strtotime('+1 month'));
            }
        } else {
            $site->backup_interval = null;
        }

        $site->save();

        return redirect()->back()->with('message', 'Site settings have been updated.');
    }

    /**
     * Refresh site plugin data.
     *
     * @param Request $request
     * @param string $siteSlug
     * @return void
     */
    protected function refreshSiteData(Request $request, $siteSlug)
    {
        $user = Auth::user();
        $site = Site::where('slug', $siteSlug)->where('user_id', $user->id)->first();

        $client = new Client();

        try {
            $client->get($site->getUpdateUrl());
            return redirect()->back()->with('message', 'Site has been update.');
        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Site could not updated.<br><small>' . $e->getMessage() . '</small>');
        }
    }

    /**
     * Delete site.
     *
     * @param Request $request
     * @param string $siteSlug
     * @return void
     */
    protected function deleteSite(Request $request, $siteSlug)
    {
        $user = Auth::user();
        $site = Site::where('slug', $siteSlug)->where('user_id', $user->id)->first();
        $siteName = $site->name;
        $site->delete();

        return redirect('/dashboard')->with('message', $siteName . ' has been deleted');
    }

    /**
     * Flag plugin to noticy admin that plugin version is missing.
     *
     * @param Request $request
     * @param int $pluginId
     * @return void
     */
    protected function reportPlugin(Request $request, $pluginId)
    {
        $plugin = Plugin::find($pluginId);
        $plugin->reported = true;
        $plugin->save();

        return redirect()->back();
    }
}
