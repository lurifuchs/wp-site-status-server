<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

use App\Variable;
use App\Events\SiteDeleted;

class Site extends \Eloquent
{
    const UPDATE_UNKNOWN = 0;
    const UPDATE_OK = 1;
    const UPDATE_WARNING = 2;
    const UPDATE_CRITICAL = 3;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'url',
        'version',
        'php_version',
        'multisite',
        'main_site',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => SiteDeleted::class,
    ];

    /**
     * @var string
     */
    private $latestWpCoreVersion = null;

    /**
     * @var string
     */
    private $latestPhpVersion = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $latestWpCoreVersion = Variable::where('key', 'wp_core_version')->first();
        if ($latestWpCoreVersion) {
            $this->latestWpCoreVersion = $latestWpCoreVersion->value;
        }

        $latestPhpVersion = Variable::where('key', 'php_version')->first();
        if ($latestPhpVersion) {
            $this->latestPhpVersion = $latestPhpVersion->value;
        }
    }

    /**
     * Add plugin to collection.
     *
     * @param \App\Plugin $plugin
     * @return void
     */
    public function setPlugins(\App\Plugin $plugin)
    {
        if (!isset($this->attributes['plugins'])) {
            $this->attributes['plugins'] = new Collection();
        }

        $this->attributes['plugins']->push($plugin);
    }

    /**
     * Get all plugins
     *
     * @return Collection
     */
    public function getPlugins()
    {
        $plugins = new Collection();

        if (isset($this->attributes['plugins'])) {
            $plugins = $this->attributes['plugins']->sortBy(function($plugin) {
                return $plugin->getStatus($this->id);
            })->reverse();
        }

        return $plugins;
    }

    /**
     * Get site update url.
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->url . '/wp-json/wp-site-status-client/update';
    }

    /**
     * Get site backup url.
     *
     * @return string
     */
    public function getBackupUrl()
    {
        return $this->url . '/wp-json/wp-site-status-client/backup';
    }

    /**
     * Get version status of WP Core.
     *
     * @return int
     */
    public function getWpStatus()
    {
        $status = self::UPDATE_OK;

        if (!$this->version) {
            $status = self::UPDATE_UNKNOWN;
        }

        if (version_compare($this->latestWpCoreVersion, $this->version) == 1) {
            $status = self::UPDATE_WARNING;
        }

        if (version_compare($this->getMajorVersion($this->latestWpCoreVersion), $this->getMajorVersion($this->version)) == 1) {
            $status = self::UPDATE_CRITICAL;
        }

        return $status;
    }

    /**
     * Get version status of WP Core as a string.
     *
     * @return string
     */
    public function getWpStatusAsString()
    {
        $status = $this->getWpStatus();
        $statusString = 'secondary';

        if ($status === self::UPDATE_OK) {
            $statusString = 'success';
        } else if ($status === self::UPDATE_WARNING) {
            $statusString = 'warning';
        } else if ($status === self::UPDATE_CRITICAL) {
            $statusString = 'danger';
        }

        return $statusString;
    }

    /**
     * Get version status for PHP.
     *
     * @return void
     */
    public function getPhpStatus()
    {
        $status = self::UPDATE_OK;

        if (!$this->php_version) {
            $status = self::UPDATE_UNKNOWN;
        }

        if (version_compare($this->latestPhpVersion, $this->php_version) == 1) {
            $status = self::UPDATE_WARNING;
        }

        if (version_compare($this->getMajorVersion($this->latestPhpVersion), $this->getMajorVersion($this->php_version)) == 1) {
            $status = self::UPDATE_CRITICAL;
        }

        return $status;
    }

    /**
     * Get version status for PHP as a string.
     *
     * @return void
     */
    public function getPhpStatusAsString()
    {
        $status = $this->getPhpStatus();
        $statusString = 'secondary';

        if ($status === self::UPDATE_OK) {
            $statusString = 'success';
        } else if ($status === self::UPDATE_WARNING) {
            $statusString = 'warning';
        } else if ($status === self::UPDATE_CRITICAL) {
            $statusString = 'danger';
        }

        return $statusString;
    }

    /**
     * Helper function that returns the major version number from semver string.
     *
     * @param string $version
     * @return void
     */
    private function getMajorVersion($version)
    {
        if (strpos($version, '.') === false) {
            return $version;
        }

        return substr($version, 0, strpos($version, '.'));
    }

    /**
     * Get backup statuses.
     */
    public function backupStatuses()
    {
        return $this->hasMany('App\BackupStatus')->orderBy('created_at', 'desc')->get();
    }
}
