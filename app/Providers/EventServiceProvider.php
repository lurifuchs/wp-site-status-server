<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        '*' => [
            \App\Listeners\SendSlackNotifications::class
        ],
        \App\Events\UserDeleted::class => [
            \App\Listeners\DeleteRelatedUserData::class,
        ],
        \App\Events\SiteDeleted::class => [
            \App\Listeners\DeleteRelatedSiteData::class,
        ],
        \App\Events\SitePluginDeleted::class => [
            //
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
