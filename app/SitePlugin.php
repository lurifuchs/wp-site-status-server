<?php

namespace App;

use App\Events\SitePluginDeleted;

class SitePlugin extends \Eloquent
{
    public $timestamps = false;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => SitePluginDeleted::class,
    ];

    /**
     * Get plugin.
     */
    public function plugin()
    {
        return $this->hasOne('App\Plugin')->first();
    }
}
