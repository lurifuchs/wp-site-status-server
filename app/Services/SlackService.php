<?php

namespace App\Services;

class SlackService
{
    public static function send($message) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, env('SLACK_URL'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'username' => 'WP Site Status',
            'text' => $message
        ]));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close($ch);
    }
}
