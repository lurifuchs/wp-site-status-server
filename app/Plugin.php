<?php

namespace App;

class Plugin extends \Eloquent
{
    /**
     * Statuses
     */
    const UPDATE_UNKNOWN = 0;
    const UPDATE_OK = 1;
    const UPDATE_WARNING = 2;
    const UPDATE_CRITICAL = 3;

    /**
     * Set plugin version for a specific site.
     *
     * @param int $siteId
     * @param string $version
     * @return void
     */
    public function setVersion($siteId, $version)
    {
        if (!isset($this->attributes['version'])) {
            $this->attributes['version'] = [];
        }

        $this->attributes['version'][$siteId] = $version;
    }

    /**
     * Get plugin version for a specific site.
     *
     * @param int $siteId
     * @return string
     */
    public function getVersion($siteId)
    {
        if (isset($this->attributes['version'][$siteId])) {
            return $this->attributes['version'][$siteId];
        } else {
            null;
        }
    }

    /**
     * Check if plugin is installed on site or network.
     *
     * @param int $siteId
     * @return void
     */
    public function isInstalled($siteId)
    {
        return $this->isInstalledOnSite($siteId) || $this->isInstalledOnNetwork($siteId);
    }

    /**
     * Set installed status for a specific site.
     *
     * @param int $siteId
     * @param bool $installed
     * @return void
     */
    public function setInstalledOnSite($siteId, $installed)
    {
        if (!isset($this->attributes['installed'])) {
            $this->attributes['installed'] = [];
        }

        $this->attributes['installed'][$siteId] = $installed;
    }

    /**
     * Check if plugin is installed on a specific site.
     *
     * @param int $siteId
     * @return void
     */
    public function isInstalledOnSite($siteId)
    {
        if (isset($this->attributes['installed'][$siteId])) {
            return (bool) $this->attributes['installed'][$siteId];
        } else {
            return false;
        }
    }

    /**
     * Set installed status for network.
     *
     * @param int $siteId
     * @param bool $installedOnNetwork
     * @return void
     */
    public function setInstalledOnNetwork($siteId, $installedOnNetwork)
    {
        if (!isset($this->attributes['installed_on_network'])) {
            $this->attributes['installed_on_network'] = [];
        }

        $this->attributes['installed_on_network'][$siteId] = $installedOnNetwork;
    }

    /**
     * Check if plugin is installed on network.
     *
     * @param int $siteId
     * @return void
     */
    public function isInstalledOnNetwork($siteId)
    {
        if (isset($this->attributes['installed_on_network'][$siteId])) {
            return (bool) $this->attributes['installed_on_network'][$siteId];
        } else {
            return false;
        }
    }

    /**
     * Get plugin update status for a specific site.
     *
     * @param int $siteId
     * @return void
     */
    public function getStatus($siteId)
    {
        $siteVersion = $this->attributes['version'][$siteId];

        $status = self::UPDATE_OK;

        if (!$this->latest_version) {
            $status = self::UPDATE_UNKNOWN;
        }

        if (version_compare($this->latest_version, $siteVersion) == 1) {
            $status = self::UPDATE_WARNING;
        }

        if (version_compare($this->getMajorVersion($this->latest_version), $this->getMajorVersion($siteVersion)) == 1) {
            $status = self::UPDATE_CRITICAL;
        }

        return $status;
    }

    /**
     * Get plugin update status for a specific site as a string.
     *
     * @param int $siteId
     * @return void
     */
    public function getStatusAsString($siteId)
    {
        $status = $this->getStatus($siteId);
        $statusString = 'secondary';

        if ($status === self::UPDATE_OK) {
            $statusString = 'success';
        } else if ($status === self::UPDATE_WARNING) {
            $statusString = 'warning';
        } else if ($status === self::UPDATE_CRITICAL) {
            $statusString = 'danger';
        }

        if (!$this->isInstalled($siteId)) {
            $statusString .= ' inactive';
        }

        return $statusString;
    }

    /**
     * Find the major version from string.
     *
     * @param string $version
     * @return string
     */
    private function getMajorVersion($version)
    {
        if (strpos($version, '.') === false) {
            return $version;
        }

        return substr($version, 0, strpos($version, '.'));
    }

    /**
     * Find the latest version of the plugin.
     *
     * @param \GuzzleHttp\Client $client
     * @return void
     */
    public function updateLatestVersion(\GuzzleHttp\Client $client, $ignoreDateCheck = false)
    {
        $now = new \DateTime();
        $updated = new \DateTime($this->updated_at);
        $interval = $now->diff($updated);
        $daysSinceUpdate = (int) $interval->format('%d');

        if (!$this->updated_at || $daysSinceUpdate > 0 || $ignoreDateCheck === true) {
            try {
                $res = $client->get('https://api.wordpress.org/plugins/info/1.0/' . $this->slug . '.json');
                $data = json_decode($res->getBody());

                if (isset($data->error)) {
                    return;
                }

                $this->latest_version = $data->version;
            } catch (\Exception $e) {
                // Flag plugins that are unable to update.
                $this->reported = 1;
                $this->save();
            }
        }
    }
}
