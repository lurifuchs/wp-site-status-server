<?php

namespace App;

class Log extends \Eloquent
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'referer',
        'status',
        'log',
    ];
}
