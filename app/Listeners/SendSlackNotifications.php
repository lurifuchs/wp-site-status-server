<?php

namespace App\Listeners;

use App\Site;

class SendSlackNotifications
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($eventName, $data)
    {
        $slackService = \App::make('App\Services\SlackService');
        $slackEvents = [
            'eloquent.created: App\\User',
            'eloquent.deleted: App\\User',
            // More...
        ];

        if (in_array($eventName, $slackEvents)) {
            $slackService->send($eventName . ' [' . $data[0]->email . ']');
        }
    }
}
