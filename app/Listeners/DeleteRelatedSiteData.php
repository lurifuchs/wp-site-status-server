<?php

namespace App\Listeners;

use App\SitePlugin;

class DeleteRelatedSiteData
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\SiteDeleted $event)
    {
        $site = $event->site;

        SitePlugin::where('site_id', $site->id)->delete();
    }
}
