<?php

namespace App\Listeners;

use App\Site;

class DeleteRelatedUserData
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\UserDeleted $event)
    {
        $user = $event->user;
        Site::where('user_id', $user->id)->delete();
    }
}
